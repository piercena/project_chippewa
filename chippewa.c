#include "chippewa.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

/* Global Variables */
char *Port		    = "9898";
char *MimeTypesPath	    = "/etc/mime.types";
char *DefaultMimeType	    = "text/plain";
char *RootPath		    = ".";
mode  ConcurrencyMode	    = SINGLE;
pthread_mutex_t PopenMutex;

/**
 *  * Display usage message.
 *   */
static void
usage(const char *progname)
{
    fprintf(stderr, "usage: %s [hcmMpr]\n", progname);
    fprintf(stderr, "options:\n");
    fprintf(stderr, "    -h            Display help message\n");
    fprintf(stderr, "    -c mode       Single, Forking, or Threaded mode\n");
    fprintf(stderr, "    -m path       Path to mimetypes file\n");
    fprintf(stderr, "    -M mimetype   Default mimetype\n");
    fprintf(stderr, "    -p port       Port to listen on\n");
    fprintf(stderr, "    -r path       Root directory\n");
}


/**
 *  * Parses command line options and starts appropriate server
 *   */
int
main(int argc, char *argv[])
{
    int c;
    int sfd;

    /* Parse command line options */
    while((c = getopt(argc, argv, "hc:m:M:p:r:")) != -1)
    {
	switch(c)
	{
	    case 'h':
		usage(argv[0]);
		return 0;
	    case 'c':
		if(strcmp(optarg, "forking")== 0){
		    ConcurrencyMode = FORKING;
		}
		if(strcmp(optarg, "threaded") == 0) 
		{
		    ConcurrencyMode = THREADED;
		}	
		break;
	    case 'm':
		MimeTypesPath = optarg;	
		break;
	    case 'M':
		DefaultMimeType = optarg;	
		break;
	    case 'p':
		Port = optarg;	
		break;
	    case 'r':
		RootPath = optarg;
		break;
	}
    }

    /* Listen to server socket */
    sfd = socket_listen(Port);
    if(sfd < 0)
    {
	printf("Cannot listen on Port: %s\n", Port);
	return EXIT_FAILURE;		
    }


    /* Determine real RootPath */
    RootPath = realpath(RootPath, NULL);

    log("Listening on port %s", Port);
    debug("RootPath        = %s", RootPath);
    debug("MimeTypesPath   = %s", MimeTypesPath);
    debug("DefaultMimeType = %s", DefaultMimeType);
    debug("ConcurrencyMode = %s", ConcurrencyMode == 0 ? "Single" : ConcurrencyMode == 1 ? "Forking" : "Threaded");

    /* Start server with specified concurrency mode */
    switch(ConcurrencyMode)
    {
	case SINGLE:
    	    single_server(sfd);
	    break;
	case FORKING:
	    forking_server(sfd);
	    break;
	case THREADED:
	    threaded_server(sfd);	
	    break;
	case UNKNOWN:
	    puts("MODE UNKNOWN");
	    return EXIT_FAILURE;	
    }
    return (EXIT_SUCCESS);





}
