/* forking.c: Forking HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>

/**
 * Handle single connection
 **/
void *
handle_threaded_request(void *arg)
{
	http_status status;
	struct request *request = (struct request *)arg;
    /* Handle request */
	status = handle_request(request);
	if(status != HTTP_STATUS_OK)
	{
		printf("Unable to process request: %s\n", strerror(errno));
	}


    /* Free Request */
	free_request(request);

    /* Exit thread */
	pthread_exit(NULL);
	}

/**
 * Spawn threads for incoming HTTP requests to handle them concurrently
 *
 * The parent should accept a request and then spawn a thread to handle the
 * request.
 **/
void
threaded_server(int sfd)
{
    struct request *request;
    pthread_t thread;

    /* Accept and handle HTTP request */
    while (1) {
	request = accept_request(sfd);
	if (request == NULL)
	{
		printf("Unable to accept client: %s\n", strerror(errno));
		continue;
	}

	/* Spawn and detach child to handle request */
	pthread_create(&thread, NULL, handle_threaded_request, request);
	pthread_detach(thread);

    }

    /* Close server socket and exit */
	close(sfd);
	}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
