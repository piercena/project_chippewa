#include "chippewa.h"

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <unistd.h>

/**
 *  * Forking version of HTTP server that utilizes a process per connection.
 *   *
 *    * The parent process accepts an incoming HTTP request and then forks a child
 *     * to handle the request.
 *      **/
void
forking_server(int sfd)
{
    struct request *request;
    pid_t pid;
    http_status status;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
	request = accept_request(sfd);
	if(request == NULL)
	{
	    printf("Unable to accept client: %s\n", strerror(errno));
	    continue;
	}
	signal(SIGCHLD, SIG_IGN);
	pid = fork();
	switch(pid)
	{
	    case -1:
		perror("fork");
		_exit(EXIT_FAILURE);
		break;
	    case 0:
    		/* Close server socket and exit */
    		close(sfd);
		status = handle_request(request);
		exit(EXIT_SUCCESS);
	    default:
		free_request(request);
	}
    }


}

