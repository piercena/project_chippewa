/* socket.c: Simple Socket Functions */

#include "chippewa.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>


int
socket_listen(const char *port)
{
    struct addrinfo  hints;
    struct addrinfo *results;
    int    socket_fd = -1;

	const char *node = NULL;
   	 /* Lookup server address information */

	memset(&hints, 0, sizeof(hints));
    	hints.ai_family   = AF_UNSPEC;		/* Use either IPv4 (AF_INET) or IPv6 (AF_INET6) */
    	hints.ai_socktype = SOCK_STREAM;	/* Use TCP  */
    	hints.ai_flags    = AI_PASSIVE;		/* Use all interfaces */

	if (getaddrinfo(node, port, &hints, &results) != 0)
	{
    		fprintf(stderr, "Could not look up on port %s: %s\n", port, strerror(errno));
    		return EXIT_FAILURE;
    	}

	/* For each server entry, allocate socket and try to connect */
    	for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
		char ip[INET6_ADDRSTRLEN];

	/* Translate IP address to string */
	if (p->ai_family == AF_INET) {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr), ip, sizeof(ip));

	} else {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in6 *)p->ai_addr)->sin6_addr), ip, sizeof(ip));
	}

	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Bind socket */
	if (bind(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to bind to %s:%s: %s\n", ip, port, strerror(errno));
	    close(socket_fd);
	    continue;
	}

	/* Listen on socket */
	if (listen(socket_fd, SOMAXCONN) < 0) {
	    fprintf(stderr, "Unable to listen on %s:%s: %s\n", ip, port, strerror(errno));
	    close(socket_fd);
	    continue;
	}

	/* Successful setup */
	fprintf(stderr, "Listening on %s:%s\n", ip, port);
	goto success;
    }

    socket_fd = -1;

success:
    freeaddrinfo(results);
    return (socket_fd);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
