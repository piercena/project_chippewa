/* single.c: Single User HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

/**
 * Single connection version of HTTP server that processes one request at a
 * time.
 **/
void
single_server(int sfd)
{
    struct request *request;
    http_status status;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
    	request = accept_request(sfd);
        if(request == NULL)
        {
            printf("Unable to accept client: %s\n", strerror(errno));
            continue;
        }

	/* Handle request */ 
   	status = handle_request(request);

        if(status != HTTP_STATUS_OK)
        {
            printf("Unable to process request: %s\n", strerror(errno));
        }

	/* Free request */
    	free_request(request);
    }

    /* Close server socket and exit */
    close(sfd);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
