CC= 	gcc
CFLAGS= -Wall -std=gnu99 -g -pthread
LIBS= 	-lm
SHELL= 	bash


all: chippewa

chippewa: chippewa.o forking.o handler.o request.o single.o socket.o threaded.o utils.o
	$(CC) $(CFLAGS) -o $@ $^

chippewa.o: chippewa.c
	$(CC) $(CFLAGS) -c -o $@ $^

forking.o: forking.c
	$(CC) $(CFLAGS) -c -o $@ $^

handler.o: handler.c
	$(CC) $(CFLAGS) -c -o $@ $^

request.o: request.c
	$(CC) $(CFLAGS) -c -o $@ $^

single.o: single.c
	$(CC) $(CFLAGS) -c -o $@ $^

socket.o: socket.c
	$(CC) $(CFLAGS) -c -o $@ $^

threaded.o: threaded.c
	$(CC) $(CFLAGS) -c -o $@ $^

utils.o: utils.c
	$(CC) $(CFLAGS) -c -o $@ $^

clean:
	rm -rf test chippewa chippewa.o forking.o handler.o request.o single.o socket.o threaded.o utils.o 
