#include "chippewa.h"

#include <errno.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

/* Internal Declarations */
http_status handle_browse_request(struct request *request);
http_status handle_file_request(struct request *request);
http_status handle_cgi_request(struct request *request);
http_status handle_error(struct request *request, http_status status);


/**
 *  * Handle HTTP Request
 *   *
 *    * This parses a request, determines the request path, determines the request
 *     * type, and then dispatches to the appropriate handler type.
 *      *
 *       * On error, handle_error should be used with an appropriate HTTP status code.
 *        **/
http_status
handle_request(struct request *r)
{
    http_status result;
    request_type type;

    /* Parse request */
    if(parse_request(r) == -1)
    {
	return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Determine request path */
    r->path = determine_request_path(r->uri);

    type = determine_request_type(r->path);

    /*dispatch to appropriate handler*/    
    switch(type)
    {
	case REQUEST_BROWSE:
		result = handle_browse_request(r);
		break;
	case REQUEST_FILE:
		result = handle_file_request(r);
		break;
	case REQUEST_CGI:
		result = handle_cgi_request(r);
		break;
	case REQUEST_BAD:
		result = handle_error(r, HTTP_STATUS_NOT_FOUND);
		break;
    }

    /*don't forget to free the string returned from determine request type*/
    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    return (result);
}

/**
 *  * Handle browse request
 *   *
 *    * This lists the contents of a directory in HTML.
 *     *
 *      * If the path cannot be opened or scanned as a directory, then handle error
 *       * with HTTP_STATUS_NOT_FOUND.
 *        **/
http_status
handle_browse_request(struct request *r)
{
    struct dirent **entries;
    int n;


    /* Open a directory for reading or scanning */
    if((n = scandir(r->path, &entries, NULL, alphasort)) == -1)
    {
	return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");
    fprintf(r->file, "Content-Type: text/html\r\n");
    fprintf(r->file, "\r\n");
    fprintf(r->file, "<html>");
    fprintf(r->file, "\t<ul>\r\n");
    
    /* For each entry in directory, emit HTML list item */
    for(int i = 0; i < n; i++)
    {
	fprintf(r->file, "\t\t<li><a href=\"%s/%s\">%s</a></li>\r\n", strcmp(r->uri, "/") ? r->uri : "", entries[i]->d_name, entries[i]->d_name);
   	free(entries[i]);
    }
    fprintf(r->file, "\t</ul>\r\n");
    fprintf(r->file, "</html>\r\n");

    free(entries);

    /* Flush socket, return OK */
    fflush(r->file);

    return (HTTP_STATUS_OK);
}

/**
 *  * Handle file request
 *   *
 *    * This opens and streams the contents of the specified file to the socket.
 *     *
 *      * If the path cannot be opened for reading, then handle error with
 *       * HTTP_STATUS_NOT_FOUND.
 *        **/
http_status
handle_file_request(struct request *r)
{
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    if((fs = fopen(r->path, "r")) == NULL)
    {
	goto fail;
    }

    /* Determine mimetype */
    mimetype = determine_mimetype(r->path);
    printf("Mimetype: %s\n", mimetype);

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");
    fprintf(r->file, "Content-Type: %s\r\n",mimetype);
    fprintf(r->file, "\r\n");

    while((nread = fread(buffer,sizeof(char), BUFSIZ,fs)) > 0)
    {
	fwrite(buffer,sizeof(char), nread,r->file);
    }

    free(mimetype); 
    fclose(fs);
    fflush(r->file);
    return (HTTP_STATUS_OK);

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    return (HTTP_STATUS_INTERNAL_SERVER_ERROR);
}

/**
 *  * Handle file request
 *   *
 *    * This popens and streams the results of the specified executables to the
 *     * socket.
 *      *
 *       *
 *        * If the path cannot be popened, then handle error with
 *         * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 *          **/
http_status
handle_cgi_request(struct request *r)
{
    FILE *pfs;
    char buffer[BUFSIZ];
    size_t nread;

    /* With PopenMutex */
        /* Export CGI environment variables from request:
 *          * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
	pthread_mutex_lock(&PopenMutex);

	setenv("DOCUMENT_ROOT", r->path, 1);
	setenv("QUERY_STRING", r->query, 1);
	setenv("REMOTE_ADDR", r->host ,1);
        setenv("REMOTE_PORT", r->port, 1);
	setenv("REMOTE_METHOD", r->method, 1);
	setenv("REQUEST_URI", r->uri, 1);
	setenv("SCRIPT_FILENAME", r->path, 1);
	setenv("SERVER_PORT", Port, 1);

	/*export environmental variables from headers*/	
	while(r->headers != NULL)
	{
	puts(r->headers->name);
	    if(strcmp(r->headers->name, "Host") == 0)
	    {
	    	setenv("HOST", r->headers->value, 1);
	    }
	    if(strcmp(r->headers->name, "User-Agent") == 0)
	    {
	    	setenv("HTTP_USER_AGENT", r->headers->value, 1);
	    }
	    if(strcmp(r->headers->name, "Accept") == 0)
	    {
	    	setenv("HTTP_ACCEPT", r->headers->value, 1);
	    }
	    if(strcmp(r->headers->name, "Accept-Language") == 0)
	    {
	    	setenv("HTTP_ACCEPT_LANGUAGE", r->headers->value, 1);
	    }
	    if(strcmp(r->headers->name, "Accept-Encoding") == 0)
	    {
	    	setenv("HTTP_ACCEPT_ENCODING", r->headers->value, 1);
	    }
	    if(strcmp(r->headers->name, "Referer") == 0)
	    {
	    	;
	    }
	    if(strcmp(r->headers->name, "Cookie") == 0)
	    {
	    	;
	    }
	    if(strcmp(r->headers->name, "Connection") == 0)
	    {
	    	setenv("HTTP_CONNECTION", r->headers->value, 1);
	    }
	    r->headers = r->headers->next;
	}



        /* Popen CGI Script */
	if((pfs = popen(r->path, "r")) == NULL)
	{
	    //handle error
	    handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	pthread_mutex_lock(&PopenMutex);
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");  


    /* Copy data from popen to socket */
    while((nread = fread(buffer, sizeof(char), BUFSIZ, pfs)) > 0)
    {
	fwrite(buffer, sizeof(char), nread, r->file);
    }

    /* Close popen, flush socket, return OK */
    pclose(pfs);
    fflush(pfs);

    return (HTTP_STATUS_OK);
}

/**
 *  * Handle displaying error page
 *   *
 *    * This writes an HTTP status error code and then generates an HTML message to
 *     * notify the user of the error.
 *      **/
http_status
handle_error(struct request *r, http_status status)
{
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    fprintf(r->file, "HTTP STATUS ERROR:\r\n");

    /* Write HTML Description of Error*/
    fprintf(r->file, "\t%s\r\n", status_string);
    fprintf(r->file, "\r\n");
    return (status);
}

